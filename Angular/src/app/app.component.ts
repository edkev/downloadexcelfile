  import { Component } from '@angular/core';
import {AppService} from './app.service';
import {utils, WorkBook, write} from 'xlsx';
import * as FileSaver from 'file-saver';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  serverMessage: string;
  private responseStatus: any;

  constructor(public appService: AppService) {}

  connectserver() {
    this.appService.connectNode()
      .subscribe(response => this.serverMessage = response.toString());
  }
  excelDownload() {
    this.appService.excelDownload()
      .subscribe(data => {
          this.responseStatus = data;
          this.generateExcelFile(data);
        },
        err => console.log(err),
        () => console.log('Request Completed222')
      );
  }
  generateExcelFile(data: any) {
    this.responseStatus = data;
    const ws_name = 'SomeSheet';
    const wb: WorkBook = { SheetNames: [], Sheets: {} };
      const ws: any = utils.json_to_sheet(parseArray(this.responseStatus.dynaModel));
    wb.SheetNames.push(ws_name);
    wb.Sheets[ws_name] = ws;
    const wbout = write(wb, { bookType: 'xlsx', bookSST: true, type: 'binary' });

    function s2ab(s) {
      const buf = new ArrayBuffer(s.length);
      const view = new Uint8Array(buf);
      for (let i = 0; i !== s.length; ++i) {
        view[i] = s.charCodeAt(i) & 0xFF;
      }
      return buf;
    }
    function parseArray(dataToParse: any) {
      const newArray = [];
      dataToParse.forEach(item => {
        Object.keys(item).forEach(key => {
          newArray.push(item[key]);
        });
      });
      console.log('newArray:' + JSON.stringify(newArray));
      return newArray;
    }

    FileSaver.saveAs(new Blob([s2ab(wbout)],
      { type: 'application/octet-stream'}),
      'exported.xlsx');
  }
}
